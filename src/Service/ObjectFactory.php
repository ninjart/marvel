<?php

namespace App\Service;

use App\Marvel\DataWrapper\AbstractDataWrapper;
use App\Marvel\Filter\AbstractFilter;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class ObjectFactory {

	private $serializer;

	public function __construct() {
		$this->serializer = new Serializer(
			array(new GetSetMethodNormalizer(), new ArrayDenormalizer()),
			array(new JsonEncoder())
		);
	}

	public function createWrapper(Response $response, string $dataWrapper): AbstractDataWrapper{
		return $this->serializer->deserialize($response->getBody(), $dataWrapper, 'json');
	}

	public function createFilter(Request $request, string $filterObject): AbstractFilter{
		$data = $request->getQueryString();
		parse_str($data, $request_array);

		return $this->serializer->deserialize(json_encode($request_array), $filterObject, 'json');
	}


}