<?php

namespace App\Controller;
use App\Marvel\Client;
use App\Marvel\DataWrapper\CharacterDataWrapper;
use App\Marvel\DataWrapper\ComicDataWrapper;
use App\Marvel\DataWrapper\CreatorDataWrapper;
use App\Marvel\DataWrapper\SeriesDataWrapper;
use App\Marvel\DataWrapper\StoryDataWrapper;
use App\Marvel\Filter\CharacterFilter;
use App\Marvel\Filter\ComicFilter;
use App\Marvel\Filter\CreatorFilter;
use App\Marvel\Filter\SeriesFilter;
use App\Marvel\Filter\StoryFilter;
use App\Service\ObjectFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/series", name="Series")
 */
class SeriesController extends DefaultController {
	/**
	 * @Route("/", name="List Series")
	 */
	public function index(ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, SeriesFilter::class);
		$response = $marvel->call("series", $filter);

		$series = $objectFactory->createWrapper($response, SeriesDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'SeriesController',
		]);
	}

	/**
	 * @Route("/{series_id}", name="Series")
	 */
	public function series(int $series_id, ObjectFactory $objectFactory) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$response = $marvel->call("series/" . $series_id);

		$series = $objectFactory->createWrapper($response, SeriesDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'SeriesController',
		]);
	}

	/**
	 * @Route("/{series_id}/characters", name="Series Characters")
	 */
	public function series_characters(int $series_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, CharacterFilter::class);
		$response = $marvel->call("series/" . $series_id. "/characters", $filter);

		$characters = $objectFactory->createWrapper($response, CharacterDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'SeriesController',
		]);
	}

	/**
	 * @Route("/{series_id}/comics", name="Series Comics")
	 */
	public function series_comics(int $series_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, ComicFilter::class);
		$response = $marvel->call("series/" . $series_id. "/comics", $filter);

		$comics = $objectFactory->createWrapper($response, ComicDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'SeriesController',
		]);
	}

	/**
	 * @Route("/{series_id}/creators", name="Series Creators")
	 */
	public function series_creators(int $series_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, CreatorFilter::class);
		$response = $marvel->call("series/" . $series_id. "/creators", $filter);

		$creators = $objectFactory->createWrapper($response, CreatorDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'SeriesController',
		]);
	}

	/**
	 * @Route("/{series_id}/events", name="Series Events")
	 */
	public function series_events(int $series_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, SeriesFilter::class);
		$response = $marvel->call("series/" . $series_id. "/events", $filter);

		$creators = $objectFactory->createWrapper($response, SeriesDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'SeriesController',
		]);
	}

	/**
	 * @Route("/{series_id}/stories", name="Series Stories")
	 */
	public function series_stories(int $series_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, StoryFilter::class);
		$response = $marvel->call("series/" . $series_id. "/stories", $filter);

		$creators = $objectFactory->createWrapper($response, StoryDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'EventController',
		]);
	}
}