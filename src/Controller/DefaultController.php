<?php

namespace App\Controller;

use App\Marvel\Client;
use App\Marvel\DataWrapper\CharacterDataWrapper;
use App\Service\ObjectFactory;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
	protected $privateApiKey = '';
	protected $publicApiKey = '';

}
