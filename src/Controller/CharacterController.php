<?php

namespace App\Controller;

use App\Marvel\Client;
use App\Marvel\DataContainer\CharacterDataContainer;
use App\Marvel\DataWrapper\CharacterDataWrapper;
use App\Marvel\DataWrapper\ComicDataWrapper;
use App\Marvel\DataWrapper\EventDataWrapper;
use App\Marvel\DataWrapper\SeriesDataWrapper;
use App\Marvel\DataWrapper\StoryDataWrapper;
use App\Marvel\Filter\CharacterFilter;
use App\Marvel\Filter\ComicFilter;
use App\Marvel\Filter\EventFilter;
use App\Marvel\Filter\SeriesFilter;
use App\Marvel\Filter\StoryFilter;
use App\Service\ObjectFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/characters", name="Characters")
 */
class CharacterController extends DefaultController {

	/**
	 * @Route("/", name="List Characters")
	 */
	public function characters(ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, CharacterFilter::class);
		$response = $marvel->call("characters", $filter);

		$characters = $objectFactory->createWrapper($response, CharacterDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'CharacterController',
		]);
	}

	/**
	 * @Route("/{character_id}", name="Character")
	 */
	public function character(int $character_id, ObjectFactory $objectFactory) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$response = $marvel->call("characters/" . $character_id);

		$character = $objectFactory->createWrapper($response, CharacterDataWrapper::class);
		return $this->render('default/index.html.twig', [
			'controller_name' => 'CharacterController',
		]);
	}

	/**
	 * @Route("/{character_id}/comics", name="Character Comics")
	 */
	public function character_comics(int $character_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, ComicFilter::class);
		$response = $marvel->call("characters/" . $character_id . "/comics", $filter);

		$comics = $objectFactory->createWrapper($response, ComicDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'DefaultController',
		]);
	}

	/**
	 * @Route("/{character_id}/events", name="Character Events")
	 */
	public function character_events(int $character_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, EventFilter::class);
		$response = $marvel->call("characters/" . $character_id . "/events", $filter);

		$events = $objectFactory->createWrapper($response, EventDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'CharacterController',
		]);
	}

	/**
	 * @Route("/{character_id}/series", name="Character Series")
	 */
	public function character_series(int $character_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, SeriesFilter::class);
		$response = $marvel->call("characters/" . $character_id . "/series", $filter);

		$series = $objectFactory->createWrapper($response, SeriesDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'CharacterController',
		]);
	}

	/**
	 * @Route("/{character_id}/stories", name="Character Stories")
	 */
	public function character_stories(int $character_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, StoryFilter::class);
		$response = $marvel->call("characters/" . $character_id . "/stories", $filter);

		$stories = $objectFactory->createWrapper($response, StoryDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'CharacterController',
		]);
	}
}
