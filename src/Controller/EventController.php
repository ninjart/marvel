<?php

namespace App\Controller;

use App\Marvel\Client;
use App\Marvel\DataWrapper\CharacterDataWrapper;
use App\Marvel\DataWrapper\ComicDataWrapper;
use App\Marvel\DataWrapper\CreatorDataWrapper;
use App\Marvel\DataWrapper\EventDataWrapper;
use App\Marvel\DataWrapper\SeriesDataWrapper;
use App\Marvel\DataWrapper\StoryDataWrapper;
use App\Marvel\Filter\CharacterFilter;
use App\Marvel\Filter\ComicFilter;
use App\Marvel\Filter\CreatorFilter;
use App\Marvel\Filter\EventFilter;
use App\Marvel\Filter\SeriesFilter;
use App\Marvel\Filter\StoryFilter;
use App\Service\ObjectFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/events", name="Events")
 */
class EventController extends DefaultController
{
	/**
	 * @Route("/", name="List Events")
	 */
	public function events(ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, EventFilter::class);
		$response = $marvel->call("events", $filter);

		$events = $objectFactory->createWrapper($response, EventDataWrapper::class);


		return $this->render('default/index.html.twig', [
			'controller_name' => 'EventController',
		]);
	}

	/**
	 * @Route("/{event_id}", name="Event")
	 */
	public function event(int $event_id, ObjectFactory $objectFactory) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$response = $marvel->call("events/" . $event_id);

		$event = $objectFactory->createWrapper($response, EventDataWrapper::class);
		var_dump($event);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'EventController',
		]);
	}

	/**
	 * @Route("/{event_id}/characters", name="Event Characters")
	 */
	public function event_characters(int $event_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, CharacterFilter::class);
		$response = $marvel->call("events/" . $event_id. "/characters", $filter);

		$characters = $objectFactory->createWrapper($response, CharacterDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'EventController',
		]);
	}

	/**
	 * @Route("/{event_id}/comics", name="Event Comics")
	 */
	public function event_comics(int $event_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, ComicFilter::class);
		$response = $marvel->call("events/" . $event_id. "/comics", $filter);

		$comics = $objectFactory->createWrapper($response, ComicDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'EventController',
		]);
	}

	/**
	 * @Route("/{event_id}/creators", name="Event Creators")
	 */
	public function event_creators(int $event_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, CreatorFilter::class);
		$response = $marvel->call("events/" . $event_id. "/creators", $filter);

		$creators = $objectFactory->createWrapper($response, CreatorDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'EventController',
		]);
	}

	/**
	 * @Route("/{event_id}/series", name="Event Series")
	 */
	public function event_series(int $event_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, SeriesFilter::class);
		$response = $marvel->call("events/" . $event_id. "/series", $filter);

		$series = $objectFactory->createWrapper($response, SeriesDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'EventController',
		]);
	}

	/**
	 * @Route("/{event_id}/stories", name="Event Stories")
	 */
	public function event_stories(int $event_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, StoryFilter::class);
		$response = $marvel->call("events/" . $event_id. "/stories", $filter);

		$series = $objectFactory->createWrapper($response, StoryDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'EventController',
		]);
	}
}
