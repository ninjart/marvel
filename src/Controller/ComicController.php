<?php

namespace App\Controller;

use App\Marvel\Client;
use App\Marvel\DataWrapper\CharacterDataWrapper;
use App\Marvel\DataWrapper\ComicDataWrapper;
use App\Marvel\DataWrapper\CreatorDataWrapper;
use App\Marvel\DataWrapper\EventDataWrapper;
use App\Marvel\DataWrapper\StoryDataWrapper;
use App\Marvel\Filter\CharacterFilter;
use App\Marvel\Filter\ComicFilter;
use App\Marvel\Filter\CreatorFilter;
use App\Marvel\Filter\EventFilter;
use App\Marvel\Filter\StoryFilter;
use App\Service\ObjectFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comics", name="Comics")
 */
class ComicController extends DefaultController {
	/**
	 * @Route("/", name="List Comics")
	 */
	public function comics(ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, ComicFilter::class);
		$response = $marvel->call("comics", $filter);

		$comics = $objectFactory->createWrapper($response, ComicDataWrapper::class);
		var_dump($comics);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}

	/**
	 * @Route("/{comic_id}", name="Comic")
	 */
	public function comic(int $comic_id, ObjectFactory $objectFactory) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$response = $marvel->call("comics/" . $comic_id);

		$comic = $objectFactory->createWrapper($response, ComicDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}

	/**
	 * @Route("/{comic_id}/characters", name="Comic Characters")
	 */
	public function comic_characters(int $comic_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, CharacterFilter::class);
		$response = $marvel->call("comics/" . $comic_id. "/characters", $filter);

		$characters = $objectFactory->createWrapper($response, CharacterDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}

	/**
	 * @Route("/{comic_id}/creators", name="Comic Creators")
	 */
	public function comic_creators(int $comic_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, CreatorFilter::class);
		$response = $marvel->call("comics/" . $comic_id. "/creators", $filter);

		$creators = $objectFactory->createWrapper($response, CreatorDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}

	/**
	 * @Route("/{comic_id}/events", name="Comic Events")
	 */
	public function comic_events(int $comic_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, EventFilter::class);
		$response = $marvel->call("comics/" . $comic_id. "/events", $filter);

		$events = $objectFactory->createWrapper($response, EventDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}

	/**
	 * @Route("/{comic_id}/stories", name="Comic Stories")
	 */
	public function comic_stories(int $comic_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, StoryFilter::class);
		$response = $marvel->call("comics/" . $comic_id. "/events", $filter);

		$stories = $objectFactory->createWrapper($response, StoryDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}
}
