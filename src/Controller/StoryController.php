<?php

namespace App\Controller;

use App\Marvel\Client;
use App\Marvel\DataWrapper\CharacterDataWrapper;
use App\Marvel\DataWrapper\ComicDataWrapper;
use App\Marvel\DataWrapper\CreatorDataWrapper;
use App\Marvel\DataWrapper\EventDataWrapper;
use App\Marvel\DataWrapper\SeriesDataWrapper;
use App\Marvel\DataWrapper\StoryDataWrapper;
use App\Marvel\Filter\CharacterFilter;
use App\Marvel\Filter\ComicFilter;
use App\Marvel\Filter\CreatorFilter;
use App\Marvel\Filter\EventFilter;
use App\Marvel\Filter\SeriesFilter;
use App\Marvel\Filter\StoryFilter;
use App\Service\ObjectFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/stories", name="Stories")
 */
class StoryController extends DefaultController {
	/**
	 * @Route("/", name="List Stories")
	 */
	public function stories(ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, StoryFilter::class);
		$response = $marvel->call("stories", $filter);

		$stories = $objectFactory->createWrapper($response, StoryDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'StoryController',
		]);
	}

	/**
	 * @Route("/{story_id}", name="Story")
	 */
	public function story(int $story_id, ObjectFactory $objectFactory) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$response = $marvel->call("stories/" . $story_id);

		$story = $objectFactory->createWrapper($response, StoryDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'StoryController',
		]);
	}

	/**
	 * @Route("/{story_id}/characters", name="Story Characters")
	 */
	public function story_characters(int $story_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, CharacterFilter::class);
		$response = $marvel->call("stories/" . $story_id. "/characters", $filter);

		$characters = $objectFactory->createWrapper($response, CharacterDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'StoryController',
		]);
	}

	/**
	 * @Route("/{story_id}/comics", name="Story Comics")
	 */
	public function story_comics(int $story_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, ComicFilter::class);
		$response = $marvel->call("stories/" . $story_id. "/comics", $filter);

		$comics = $objectFactory->createWrapper($response, ComicDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'StoryController',
		]);
	}

	/**
	 * @Route("/{story_id}/creators", name="Story Creators")
	 */
	public function story_creators(int $story_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, CreatorFilter::class);
		$response = $marvel->call("stories/" . $story_id. "/creators", $filter);

		$creators = $objectFactory->createWrapper($response, CreatorDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'StoryController',
		]);
	}

	/**
	 * @Route("/{story_id}/events", name="Story Events")
	 */
	public function story_events(int $story_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, EventFilter::class);
		$response = $marvel->call("stories/" . $story_id. "/creators", $filter);

		$creators = $objectFactory->createWrapper($response, EventDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'StoryController',
		]);
	}

	/**
	 * @Route("/{story_id}/series", name="Story Series")
	 */
	public function story_series(int $story_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, SeriesFilter::class);
		$response = $marvel->call("stories/" . $story_id. "/series", $filter);

		$series = $objectFactory->createWrapper($response, SeriesDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'StoryController',
		]);
	}
}
