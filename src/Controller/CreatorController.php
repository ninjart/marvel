<?php

namespace App\Controller;

use App\Marvel\Client;
use App\Marvel\DataWrapper\ComicDataWrapper;
use App\Marvel\DataWrapper\CreatorDataWrapper;
use App\Marvel\DataWrapper\EventDataWrapper;
use App\Marvel\DataWrapper\SeriesDataWrapper;
use App\Marvel\DataWrapper\StoryDataWrapper;
use App\Marvel\Filter\ComicFilter;
use App\Marvel\Filter\CreatorFilter;
use App\Marvel\Filter\EventFilter;
use App\Marvel\Filter\SeriesFilter;
use App\Marvel\Filter\StoryFilter;
use App\Service\ObjectFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/creators", name="Creators")
 */
class CreatorController extends DefaultController
{
	/**
	 * @Route("/", name="List Creators")
	 */
	public function creators(ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, CreatorFilter::class);
		$response = $marvel->call("creators", $filter);

		$creators = $objectFactory->createWrapper($response, CreatorDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'CreatorController',
		]);
	}

	/**
	 * @Route("/{creator_id}", name="Creator")
	 */
	public function creator(int $creator_id, ObjectFactory $objectFactory) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$response = $marvel->call("creators/" . $creator_id);

		$creator = $objectFactory->createWrapper($response, CreatorDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}

	/**
	 * @Route("/{creator_id}/comics", name="Creator Comics")
	 */
	public function creator_comics(int $creator_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, ComicFilter::class);
		$response = $marvel->call("creators/" . $creator_id. "/comics", $filter);

		$comics = $objectFactory->createWrapper($response, ComicDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}

	/**
	 * @Route("/{creator_id}/events", name="Creator Events")
	 */
	public function creator_events(int $creator_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, EventFilter::class);
		$response = $marvel->call("creators/" . $creator_id. "/comics", $filter);

		$events = $objectFactory->createWrapper($response, EventDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}

	/**
	 * @Route("/{creator_id}/series", name="Creator Series")
	 */
	public function creator_series(int $creator_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, SeriesFilter::class);
		$response = $marvel->call("creators/" . $creator_id. "/series", $filter);

		$events = $objectFactory->createWrapper($response, SeriesDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}

	/**
	 * @Route("/{creator_id}/stories", name="Creator Stories")
	 */
	public function creator_stories(int $creator_id, ObjectFactory $objectFactory, Request $request) {
		$marvel = new Client($this->privateApiKey, $this->publicApiKey);
		$filter = $objectFactory->createFilter($request, StoryFilter::class);
		$response = $marvel->call("creators/" . $creator_id. "/stoires", $filter);

		$stories = $objectFactory->createWrapper($response, StoryDataWrapper::class);

		return $this->render('default/index.html.twig', [
			'controller_name' => 'ComicController',
		]);
	}
}
