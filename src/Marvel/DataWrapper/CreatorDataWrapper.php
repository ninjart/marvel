<?php

namespace App\Marvel\DataWrapper;


use App\Marvel\DataContainer\CreatorDataContainer;

class CreatorDataWrapper extends AbstractDataWrapper {

	private $data;

	public function getData(): CreatorDataContainer {
		return $this->data;
	}

	public function setData(array $data) {
		$this->data = $data;
	}

}