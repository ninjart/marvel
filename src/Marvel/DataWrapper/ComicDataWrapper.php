<?php

namespace App\Marvel\DataWrapper;

use App\Marvel\DataContainer\ComicDataContainer;

class ComicDataWrapper extends AbstractDataWrapper {

	private $data;

	public function getData(): ComicDataContainer {
		return $this->data;
	}

	public function setData(array $data) {
		$this->data = $data;
	}

}