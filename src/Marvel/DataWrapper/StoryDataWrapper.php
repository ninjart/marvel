<?php

namespace App\Marvel\DataWrapper;

use App\Marvel\DataContainer\StoryDataContainer;

class StoryDataWrapper extends AbstractDataWrapper {

	private $data;

	public function getData(): StoryDataContainer {
		return $this->data;
	}

	public function setData(array $data) {
		$this->data = $data;
	}

}