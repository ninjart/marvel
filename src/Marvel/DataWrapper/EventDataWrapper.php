<?php

namespace App\Marvel\DataWrapper;

use App\Marvel\DataContainer\EventDataContainer;

class EventDataWrapper extends AbstractDataWrapper{
	private $data;

	public function getData(): EventDataContainer {
		return $this->data;
	}

	public function setData(array $data) {
		$this->data = $data;
	}

}