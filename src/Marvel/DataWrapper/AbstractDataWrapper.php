<?php

namespace App\Marvel\DataWrapper;


abstract class AbstractDataWrapper{
	private $code;
	private $status;
	private $copyright;
	private $attributionText;
	private $attributionHTML;
	private $etag;

	public function getCode(): int{
		return $this->code;
	}

	public function getStatus(): string{
		return $this->status;
	}

	public function getCopyright(): string{
		return $this->copyright;
	}

	public function getAttributionText(): string{
		return $this->attributionText;
	}

	public function getAttributionHTML(): string{
		return $this->attributionHTML;
	}

	public function getEtag(): string{
		return $this->etag;
	}

	public function setCode(int $code) {
		$this->code = $code;
	}

	public function setStatus(string $status) {
		$this->status = $status;
	}

	public function setCopyright(string $copyright) {
		$this->copyright = $copyright;
	}

	public function setAttributionText(string $attributionText) {
		$this->attributionText = $attributionText;
	}

	public function setAttributionHTML(string $attributionHTML) {
		$this->attributionHTML = $attributionHTML;
	}

	public function setEtag(string $etag) {
		$this->etag = $etag;
	}
}