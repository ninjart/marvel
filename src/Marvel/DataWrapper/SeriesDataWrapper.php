<?php

namespace App\Marvel\DataWrapper;

use App\Marvel\DataContainer\SeriesDataContainer;

class SeriesDataWrapper extends AbstractDataWrapper {

	private $data;

	public function getData(): SeriesDataContainer {
		return $this->data;
	}

	public function setData(array $data) {
		$this->data = $data;
	}

}