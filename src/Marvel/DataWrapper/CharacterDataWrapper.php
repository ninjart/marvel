<?php

namespace App\Marvel\DataWrapper;


use App\Marvel\DataContainer\CharacterDataContainer;

class CharacterDataWrapper extends AbstractDataWrapper {

	private $data;

	public function getData(): CharacterDataContainer {
		return $this->data;
	}

	public function setData(array $data) {

		$this->data = $data;
	}

}