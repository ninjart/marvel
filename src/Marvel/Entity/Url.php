<?php

namespace App\Marvel\Entity;


class Url {

	private $type;
	private $url;

	public function getType(): string {
		return $this->type;
	}

	public function getUrl(): string {
		return $this->url;
	}

	public function setType(string $type) {
		$this->type = $type;
	}

	public function setUrl(string $url) {
		$this->url = $url;
	}
}