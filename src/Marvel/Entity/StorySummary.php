<?php

namespace App\Marvel\Entity;

class StorySummary {
	private $resourceURI;
	private $name;
	private $type;

	public function getResourceURI(): string {
		return $this->resourceURI;
	}

	public function getName(): string {
		return $this->name;
	}

	public function getType(): string {
		return $this->type;
	}

	public function setResourceURI(string $resourceURI) {
		$this->resourceURI = $resourceURI;
	}

	public function setName(string $name) {
		$this->name = $name;
	}

	public function setType(string $type) {
		$this->type = $type;
	}
}