<?php

namespace App\Marvel\Entity;


class TextObject {

	private $type;
	private $language;
	private $text;

	public function getType(): string {
		return $this->type;
	}

	public function getLanguage(): string {
		return $this->language;
	}

	public function getText(): string {
		return $this->text;
	}

	public function setType(string $type) {
		$this->type = $type;
	}

	public function setLanguage(string $language) {
		$this->language = $language;
	}

	public function setText(string $text) {
		$this->text = $text;
	}
}