<?php

namespace App\Marvel\Entity;

class CharacterSummary {
	private $resourceURI;
	private $name;
	private $role;

	public function getResourceURI(): string {
		return $this->resourceURI;
	}

	public function getName(): string {
		return $this->name;
	}

	public function getRole(): string {
		return $this->role;
	}

	public function setResourceURI(string $resourceURI) {
		$this->resourceURI = $resourceURI;
	}

	public function setName(string $name) {
		$this->name = $name;
	}

	public function setRole(string $role) {
		$this->role = $role;
	}
}