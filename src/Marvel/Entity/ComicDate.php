<?php

namespace App\Marvel\Entity;


class ComicDate {

	private $type;
	private $date;

	public function getType(): string {
		return $this->type;
	}

	public function getDate(): string {
		return $this->date;
	}

	public function setType(string $type) {
		$this->type = $type;
	}

	public function setDate(string $date) {
		$this->date = $date;
	}
}