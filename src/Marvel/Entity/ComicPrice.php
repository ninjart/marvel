<?php

namespace App\Marvel\Entity;


class ComicPrice {

	private $type;
	private $price;

	public function getType(): string {
		return $this->type;
	}

	public function getPrice(): float {
		return $this->price;
	}

	public function setType(string $type) {
		$this->type = $type;
	}

	public function setPrice(float $price) {
		$this->price = $price;
	}
}