<?php

namespace App\Marvel\Entity;


class Series {

	private $id;
	private $title;
	private $description;
	private $resourceURI;
	private $urls;
	private $startYear;
	private $endYear;
	private $rating;
	private $modified;
	private $thumbnail;
	private $comics;
	private $stories;
	private $events;
	private $characters;
	private $creators;
	private $next;
	private $previous;

	public function getId(): int {
		return $this->id;
	}

	public function getTitle(): string {
		return $this->title;
	}

	public function getDescription(): string {
		return $this->description;
	}

	public function getResourceURI(): string {
		return $this->resourceURI;
	}

	public function getUrls(): array {
		return $this->urls;
	}

	public function getStartYear(): string {
		return $this->startYear;
	}

	public function getEndYear(): string {
		return $this->endYear;
	}

	public function getModified(): string {
		return $this->modified;
	}

	public function getRating(): string {
		return $this->rating;
	}

	public function getThumbnail(): Image {
		return $this->thumbnail;
	}

	public function getComics(): ComicList {
		return $this->comics;
	}

	public function getStories(): StoryList {
		return $this->stories;
	}

	public function getEvents(): EventList {
		return $this->events;
	}

	public function getCharacters(): CharacterList {
		return $this->characters;
	}

	public function getCreators(): CreatorList {
		return $this->creators;
	}

	public function getNext(): EventSummary {
		return $this->next;
	}

	public function getPrevious(): EventSummary {
		return $this->previous;
	}

	public function setID(int $id) {
		$this->id = $id;
	}

	public function setTitle(string $title) {
		$this->title = $title;
	}

	public function setDescription(string $description) {
		$this->description = $description;
	}

	public function setResourceURI(string $resourceURI) {
		$this->resourceURI = $resourceURI;
	}

	public function setUrls(array $urls) {
		$this->urls = $urls;
	}

	public function setStartYear(string $startYear) {
		$this->startYear = $startYear;
	}

	public function setEndYear(string $endYear) {
		$this->endYear = $endYear;
	}

	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	public function setThumbnail(Image $thumbnail) {
		$this->thumbnail = $thumbnail;
	}

	public function setComics(ComicList $comics) {
		$this->comics = $comics;
	}

	public function setStories(StoryList $stories) {
		$this->stories = $stories;
	}

	public function setEvents(EventList $events) {
		$this->events = $events;
	}

	public function setCharacters(CharacterList $characters) {
		$this->characters = $characters;
	}

	public function stCreators(CreatorList $creators) {
		$this->creators = $creators;
	}

	public function setNext(EventSummary $next) {
		$this->next = $next;
	}

	public function setPrevious(EventSummary $previous) {
		$this->previous = $previous;
	}
}