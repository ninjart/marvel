<?php

namespace App\Marvel\Entity;


class Creator {

	private $id;
	private $firstName;
	private $middleName;
	private $lastName;
	private $suffix;
	private $fullName;
	private $modified;
	private $resourceURI;
	private $urls;
	private $thumbnail;
	private $series;
	private $stories;
	private $comics;
	private $events;

	public function getId(): int {
		return $this->id;
	}

	public function getFirstName(): string {
		return $this->firstName;
	}

	public function getMiddleName(): string {
		return $this->middleName;
	}

	public function getLastName(): string {
		return $this->lastName;
	}

	public function getSuffix(): string {
		return $this->suffix;
	}

	public function getFullName(): string {
		return $this->fullName;
	}

	public function getModified(): string{
		return $this->modified;
	}

	public function getResourceURI(): string {
		return $this->resourceURI;
	}

	public function getUrls(): array {
		return $this->urls;
	}

	public function getThumbnail(): Image {
		return $this->thumbnail;
	}

	public function getSeries(): SeriesList {
		return $this->series;
	}

	public function getStories(): StoryList {
		return $this->stories;
	}

	public function getComics(): ComicList {
		return $this->comics;
	}

	public function getEvents(): EventList {
		return $this->events;
	}

	public function setID(int $id) {
		$this->id = $id;
	}

	public function setFirstName(string $FirstName) {
		$this->firstName = $FirstName;
	}

	public function setMiddleName(string $middleName) {
		$this->middleName = $middleName;
	}

	public function setlastName(string $lastName) {
		$this->lastName = $lastName;
	}

	public function setSuffix(string $suffix) {
		$this->suffix = $suffix;
	}

	public function setfullName(string $fullName) {
		$this->fullName = $fullName;
	}

	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	public function setResourceURI(string $resourceURI) {
		$this->resourceURI = $resourceURI;
	}

	public function setUrls(array $urls) {
		$this->urls = $urls;
	}

	public function setThumbnail(Image $thumbnail) {
		$this->thumbnail = $thumbnail;
	}

	public function setSeries(SeriesList $series) {
		$this->series = $series;
	}

	public function setStories(StoryList $stories) {
		$this->stories = $stories;
	}

	public function setComics(ComicList $comics) {
		$this->comics = $comics;
	}

	public function setEvents(EventList $events) {
		$this->events = $events;
	}
}