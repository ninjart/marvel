<?php

namespace App\Marvel\Entity;


class Image {

	private $path;
	private $extension;

	public function getPath(): string {
		return $this->path;
	}

	public function getExtension(): string {
		return $this->extension;
	}

	public function setPath(string $path) {
		$this->path = $path;
	}

	public function setExtention(string $extension) {
		$this->extentsion = $extension;
	}

}