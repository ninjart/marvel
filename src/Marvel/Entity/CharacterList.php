<?php

namespace App\Marvel\Entity;


class CharacterList extends AbstractList {
	private $items;


	public function getItems(): array {
		return $this->items;
	}

	public function setItems(array $items) {
		$this->items = $items;
	}
}