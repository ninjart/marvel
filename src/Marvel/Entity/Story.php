<?php

namespace App\Marvel\Entity;


class Story {
	private $id;
	private $title;
	private $description;
	private $resourceURI;
	private $type;
	private $modified;
	private $thumbnail;
	private $comics;
	private $series;
	private $events;
	private $characters;
	private $creators;
	private $originalissue;

	public function getId(): int {
		return $this->id;
	}

	public function getTitle(): string {
		return $this->title;
	}

	public function getDescription(): string {
		return $this->description;
	}

	public function getResourceURI(): string {
		return $this->resourceURI;
	}

	public function getType(): string {
		return $this->type;
	}

	public function getModified(): string {
		return $this->modified;
	}

	public function getThumbnail(): Image {
		return $this->thumbnail;
	}

	public function getComics(): ComicList {
		return $this->comics;
	}

	public function getSeries(): SeriesList {
		return $this->series;
	}

	public function getEvents(): EventList {
		return $this->events;
	}

	public function getCharacters(): CharacterList {
		return $this->characters;
	}

	public function getCreators(): CreatorList {
		return $this->creators;
	}

	public function getOriginalissue(): ComicSummary {
		return $this->originalissue;
	}

	public function setID(int $id) {
		$this->id = $id;
	}

	public function setTitle(string $title) {
		$this->title = $title;
	}

	public function setDescription(string $description) {
		$this->description = $description;
	}

	public function setResourceURI(string $resourceURI) {
		$this->resourceURI = $resourceURI;
	}

	public function setType(string $type) {
		$this->type = $type;
	}

	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	public function setThumbnail(Image $thumbnail) {
		$this->thumbnail = $thumbnail;
	}

	public function setComics(ComicList $comics) {
		$this->comics = $comics;
	}

	public function setSeries(SeriesList $series) {
		$this->series = $series;
	}

	public function setEvents(EventList $events) {
		$this->events = $events;
	}

	public function setCharacters(CharacterList $characters) {
		$this->characters = $characters;
	}

	public function setCreators(CreatorList $creators) {
		$this->creators = $creators;
	}

	public function setOriginalissue(ComicSummary $originalissue) {
		$this->originalissue = $originalissue;
	}

}