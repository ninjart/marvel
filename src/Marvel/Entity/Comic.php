<?php

namespace App\Marvel\Entity;


class Comic {

	private $id;
	private $digitialId;
	private $title;
	private $issueNumber;
	private $variantDescription;
	private $description;
	private $modified;
	private $isbn;
	private $upc;
	private $diamondCode;
	private $ean;
	private $issn;
	private $format;
	private $pageCount;
	private $textObjects;
	private $resourceURI;
	private $urls;
	private $series;
	private $variants;
	private $collections;
	private $collectedIssues;
	private $dates;
	private $prices;
	private $thumbnail;
	private $images;
	private $creators;
	private $characters;
	private $stories;
	private $events;

	public function getId(): int {
		return $this->id;
	}

	public function getDigitalId(): int {
		return $this->digitialId;
	}

	public function getTitle(): string {
		return $this->title;
	}

	public function getIssueNumber(): float {
		return $this->issueNumber;
	}

	public function getVariantDescription(): string {
		return $this->variantDescription;
	}

	public function getDescription(): string {
		return $this->description;
	}

	public function getModified(): string{
		return $this->modified;
	}

	public function getIsbn(): string {
		return $this->isbn;
	}

	public function getUpc(): string{
		return $this->upc;
	}

	public function getDiamondCode(): string {
		return $this->diamondCode;
	}

	public function getEan(): string {
		return $this->ean;
	}

	public function getIssn(): string {
		return $this->issn;
	}

	public function getFormat(): string {
		return $this->format;
	}

	public function getPageCount(): int {
		return $this->pageCount;
	}

	public function getTextObjects(): array {
		return $this->textObjects;
	}

	public function getResourceURI(): string {
		return $this->resourceURI;
	}

	public function getUrls(): array {
		return $this->urls;
	}

	public function getSeries(): SeriesSummary {
		return $this->series;
	}

	public function getVariants(): array {
		return $this->variants;
	}

	public function getCollections(): array {
		return $this->collections;
	}

	public function getCollectedIssues(): array {
		return $this->collectedIssues;
	}

	public function getDates(): array {
		return $this->dates;
	}

	public function getPrices(): array {
		return $this->prices;
	}

	public function getThumbnail(): Image {
		return $this->thumbnail;
	}

	public function getImages(): array {
		return $this->images;
	}

	public function getCreators(): CreatorList {
		return $this->creators;
	}

	public function getCharacters(): CharacterList {
		return $this->characters;
	}

	public function getStories(): StoryList {
		return $this->stories;
	}

	public function getEvents(): EventList {
		return $this->events;
	}

	public function setId(int $id) {
		$this->id = $id;
	}

	public function setDigitalId(int $digitalId) {
		$this->digitalId = $digitalId;
	}

	public function setTitle(string $title) {
		$this->title = $title;
	}

	public function setIssueNumber(float $issueNumber) {
		$this->issueNumber = $issueNumber;
	}

	public function setVariantDescription(string $variantDescription) {
		$this->variantDescription = $variantDescription;
	}

	public function setDescription(string $description) {
		$this->description = $description;
	}

	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	public function setIsbn(string $isbn) {
		$this->isbn = $isbn;
	}

	public function setUpc(string $upc) {
		$this->upc = $upc;
	}

	public function setDiamondCode(string $diamondCode) {
		$this->diamondCode = $diamondCode;
	}

	public function setEan(string $ean) {
		$this->ean = $ean;
	}

	public function setIssn(string $issn) {
		$this->issn = $issn;
	}

	public function setFormat(string $format) {
		$this->format = $format;
	}

	public function setPageCount(int $pageCount) {
		$this->pageCount = $pageCount;
	}

	public function setTextObjects(array $textObjects) {
		$this->textObjects = $textObjects;
	}

	public function setResourceURI(string $resourceURI) {
		$this->resourceURI = $resourceURI;
	}

	public function setUrls(array $urls) {
		$this->urls = $urls;
	}

	public function setSeries(SeriesSummary $series) {
		$this->series = $series;
	}

	public function setVariants(array $variants) {
		$this->variants = $variants;
	}

	public function setCollections(array $collections) {
		$this->collections = $collections;
	}

	public function setCollectedIssues(array $collectedIssues) {
		$this->collectedIssues = $collectedIssues;
	}

	public function setDates(array $dates) {
		$this->dates = $dates;
	}

	public function setPrices(array $prices) {
		$this->prices = $prices;
	}

	public function setThumbnail(Image $thumbnail) {
		$this->thumbnail = $thumbnail;
	}

	public function setImages(array $images) {
		$this->images = $images;
	}

	public function setCreators(CreatorList $creators) {
		$this->creators = $creators;
	}

	public function setCharacters(CharacterList $characters) {
		$this->characters = $characters;
	}

	public function setStories(StoryList $stories) {
		$this->stories = $stories;
	}

	public function setEvents(EventList $events) {
		$this->events = $events;
	}
}