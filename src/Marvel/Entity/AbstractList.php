<?php

namespace App\Marvel\Entity;


class AbstractList {
	private $available;
	private $returned;
	private $collectionURI;

	public function getAvailable(): int {
		return $this->available;
	}

	public function getReturned(): int {
		return $this->returned;
	}

	public function getCollectionUrI(): string {
		return $this->collectionURI;
	}

	public function setAvailable(int $available) {
		$this->available = $available;
	}

	public function setReturned(int $returned) {
		$this->returned = $returned;
	}

	public function setCollectionURI(string $collectionURI) {
		$this->collectionURI = $collectionURI;
	}

}