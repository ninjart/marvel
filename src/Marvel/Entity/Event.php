<?php

namespace App\Marvel\Entity;


class Event {
	private $id;
	private $title;
	private $description;
	private $resourceURI;
	private $urls;
	private $modified;
	private $start;
	private $end;
	private $thumbnail;
	private $comics;
	private $stories;
	private $series;
	private $characters;
	private $creators;
	private $next;
	private $previous;

	public function getId(): int {
		return $this->id;
	}

	public function getTitle(): string {
		return $this->title;
	}

	public function getDescription(): string {
		return $this->description;
	}

	public function getResourceURI(): string {
		return $this->resourceURI;
	}

	public function getUrls(): array {
		return $this->urls;
	}

	public function getModified(): string {
		return $this->modified;
	}

	public function getStart(): string {
		return $this->start;
	}

	public function getEnd(): string {
		return $this->end;
	}

	public function getThumbnail(): Image {
		return $this->thumbnail;
	}

	public function getComics(): ComicList {
		return $this->comics;
	}

	public function getStories(): StoryList {
		return $this->stories;
	}

	public function getSeries(): SeriesList {
		return $this->series;
	}

	public function getCharacters(): CharacterList {
		return $this->characters;
	}

	public function getCreators(): CreatorList {
		return $this->creators;
	}

	public function getNext(): EventSummary {
		return $this->next;
	}

	public function getPrevious(): EventSummary {
		return $this->previous;
	}

	public function setID(int $id) {
		$this->id = $id;
	}

	public function setTitle(string $title) {
		$this->title = $title;
	}

	public function setDescription(string $description) {
		$this->description = $description;
	}

	public function setResourceURI(string $resourceURI) {
		$this->resourceURI = $resourceURI;
	}

	public function setUrls(array $urls) {
		$this->urls = $urls;
	}

	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	public function setStart(string $start) {
		$this->start = $start;
	}

	public function setEnd(string $end) {
		$this->end = $end;
	}

	public function setThumbnail(Image $thumbnail) {
		$this->thumbnail = $thumbnail;
	}

	public function setComics(ComicList $comics) {
		$this->comics = $comics;
	}

	public function setStories(StoryList $stories) {
		$this->stories = $stories;
	}

	public function setSeries(SeriesList $series) {
		$this->series = $series;
	}

	public function setCharacters(CharacterList $characters) {
		$this->characters = $characters;
	}

	public function setCreators(CreatorList $creators) {
		$this->creators = $creators;
	}

	public function setNext(EventSummary $next) {
		$this->next = $next;
	}

	public function setPrevious(EventSummary $previous) {
		$this->previous = $previous;
	}

}