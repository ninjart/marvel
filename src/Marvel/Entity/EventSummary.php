<?php

namespace App\Marvel\Entity;

class EventSummary {
	private $resourceURI;
	private $name;

	public function getResourceURI(): string {
		return $this->resourceURI;
	}

	public function getName(): string {
		return $this->name;
	}

	public function setResourceURI(string $resourceURI) {
		$this->resourceURI = $resourceURI;
	}

	public function setName(string $name) {
		$this->name = $name;
	}
}