<?php

namespace App\Marvel\Entity;


use Doctrine\Common\Collections\ArrayCollection;

class Character {

	private $id;
	private $name;
	private $description;
	private $modified;
	private $resourceURI;
	private $urls;
	private $thumbnail;
	private $comics;
	private $stories;
	private $events;
	private $series;

	public function getId(): int {
		return $this->id;
	}

	public function getName(): string {
		return $this->name;
	}

	public function getDescription(): string {
		return $this->description;
	}

	public function getModified(): string {
		return $this->modified;
	}

	public function getResourceURI(): string {
		return $this->resourceURI;
	}

	public function getUrls(): ArrayCollection {
		return $this->urls;
	}

	public function getThumbnails(): Image {
		return $this->thumbnail;
	}

	public function getComics(): ComicList {
		return $this->comics;
	}

	public function getStories(): StoryList {
		return $this->stories;
	}

	public function getEvents(): EventList {
		return $this->events;
	}

	public function getSeries(): SeriesList {
		return $this->series;
	}

	public function setId(int $id) {
		$this->id = $id;
	}

	public function setName(string $name) {
		$this->name = $name;
	}

	public function setDescription(string $description) {
		$this->description = $description;
	}

	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	public function setResourceURI(string $resourceURI) {
		$this->resourceURI = $resourceURI;
	}

	public function setUrls(ArrayCollection $urls) {
		$this->urls = $urls;
	}

	public function setThumbnail(Image $thumbnail) {
		$this->thumbnail = $thumbnail;
	}

	public function setComics(ComicList $comics) {
		$this->comics = $comics;
	}

	public function setStories(StoryList $stories) {
		$this->stories = $stories;
	}

	public function setEvents(EventList $events) {
		$this->events = $events;
	}

	public function setSeries(SeriesList $series) {
		$this->series = $series;
	}
}