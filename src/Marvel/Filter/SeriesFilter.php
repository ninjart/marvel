<?php

namespace App\Marvel\Filter;


class SeriesFilter extends AbstractFilter {

	public $name;
	public $titleStartsWith;
	public $startYear;
	public $modifiedSince;
	public $comics;
	public $stories;
	public $events;
	public $creators;
	public $characters;
	public $seriesType;
	public $contains;

	public function getName(): string {
		return $this->name;
	}

	public function getTitleStartsWith(): string {
		return $this->titleStartsWith;
	}

	public function getStartYear(): int {
		return $this->startYear;
	}

	public function getModifiedSince(): string {
		return $this->modifiedSince;
	}

	public function getComics(): string {
		return $this->comics;
	}

	public function getStories(): string {
		return $this->stories;
	}

	public function getEvents(): string {
		return $this->events;
	}

	public function getCreators(): string {
		return $this->creators;
	}

	public function getCharacters(): string {
		return $this->characters;
	}

	public function getSeriesType(): string {
		return $this->seriesType;
	}

	public function getContains(): string {
		return $this->contains;
	}

	public function setName(string $name)
	{
		$this->name = $name;
	}

	public function setTitleStartsWith(string $titleStartsWith) {
		$this->titleStartsWith = $titleStartsWith;
	}

	public function setStartYear(int $startYear) {
		$this->startYear = $startYear;
	}

	public function setModifiedSince(string $modifiedSince) {
		$this->modifiedSince = $modifiedSince;
	}

	public function setComics(string $comics) {
		$this->comics = $comics;
	}

	public function setStories(string $stories) {
		$this->stories = $stories;
	}

	public function setEvents(string $events) {
		$this->events = $events;
	}

	public function setCreators(string $creators) {
		$this->creators = $creators;
	}

	public function setCharacters(string $characters) {
		$this->characters = $characters;
	}

	public function setSeriesType(string $seriesType) {
		$this->seriesType = $seriesType;
	}

	public function setContains(string $contains) {
		$this->contains = $contains;
	}

}