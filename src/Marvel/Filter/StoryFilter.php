<?php

namespace App\Marvel\Filter;


class StoryFilter extends AbstractFilter {

	public $modifiedSince;
	public $comics;
	public $series;
	public $events;
	public $creators;
	public $characters;

	public function getModifiedSince(): string {
		return $this->modifiedSince;
	}

	public function getComics(): string {
		return $this->comics;
	}

	public function getSeries(): string {
		return $this->series;
	}

	public function getEvents(): string {
		return $this->events;
	}

	public function getCreators(): string {
		return $this->creators;
	}

	public function getCharacters(): string {
		return $this->characters;
	}

	public function setModifiedSince(string $modifiedSince) {
		$this->modifiedSince = $modifiedSince;
	}

	public function setComics(string $comics) {
		$this->comics = $comics;
	}

	public function setSeries(string $series) {
		$this->series = $series;
	}

	public function setEvents(string $events) {
		$this->events = $events;
	}

	public function setCreators(string $creators) {
		$this->creators = $creators;
	}

	public function setCharacters(string $characters) {
		$this->characters = $characters;
	}

}