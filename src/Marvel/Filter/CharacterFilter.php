<?php

namespace App\Marvel\Filter;

class CharacterFilter extends AbstractFilter {

	public $name;
	public $nameStartsWith;
	public $modifiedSince;
	public $comics;
	public $series;
	public $events;
	public $stories;


	public function getName(): string {
		return $this->name;
	}

	public function getNameStartsWith(): string {
		return $this->nameStartsWith;
	}

	public function getModifiedSince(): string {
		return $this->modifiedSince;
	}

	public function getSeries(): int {
		return $this->series;
	}

	public function getEvents(): string {
		return $this->events;
	}

	public function getStories(): string {
		return $this->stories;
	}


	public function setName(string $name) {
		$this->name = $name;
	}

	public function setNameStartsWith(string $nameStartsWith) {
		$this->nameStartsWith = $nameStartsWith;
	}

	public function setModifiedSince(string $modifiedSince) {
		$this->modifiedSince = $modifiedSince;
	}

	public function setSeries(int $series) {
		$this->series = $series;
	}

	public function setEvents(string $events) {
		$this->events = $events;
	}

	public function setStories(string $stories) {
		$this->stories = $stories;
	}

}