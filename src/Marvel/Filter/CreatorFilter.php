<?php

namespace App\Marvel\Filter;


class CreatorFilter extends AbstractFilter{

	public $firstName;
	public $middleName;
	public $lastName;
	public $suffix;
	public $nameStartsWith;
	public $firstNameStartsWith;
	public $middleNameStartsWith;
	public $lastNameStartsWith;
	public $modifiedSince;
	public $comics;
	public $series;
	public $events;
	public $stories;
	public $orderBy;

	public function getFirstName(): string {
		return $this->firstName;
	}

	public function getMiddleName(): string {
		return $this->middleName;
	}

	public function getLastName(): string {
		return $this->lastName;
	}

	public function getSuffix(): string {
		return $this->suffix;
	}

	public function getNameStartsWith(): string {
		return $this->nameStartsWith;
	}

	public function getFirstNameStartsWith(): string {
		return $this->firstNameStartsWith;
	}

	public function getMiddleNameStartsWith(): string {
		return $this->middleNameStartsWith;
	}

	public function getLastNameStartsWith(): string {
		return $this->lastNameStartsWith;
	}

	public function getModifiedSince():string {
		return $this->modifiedSince;
	}

	public function getComics(): int {
		return $this->comics;
	}

	public function getSeries(): string {
		return $this->series;
	}

	public function getEvents(): string {
		return $this->events;
	}

	public function getStories(): string {
		return $this->stories;
	}


	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	public function setMiddleName($middleName) {
		$this->middleName = $middleName;
	}

	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	public function setSuffix(string $suffix) {
		$this->suffix = $suffix;
	}

	public function setNameStartsWith(string $nameStartsWith) {
		$this->nameStartsWith = $nameStartsWith;
	}

	public function setFirstNameStartsWith(string $firstNameStartsWith) {
		$this->firstNameStartsWith = $firstNameStartsWith;
	}

	public function setMiddleNameStartsWith(string $middleNameStartsWith) {
		$this->middleNameStartsWith = $middleNameStartsWith;
	}

	public function setLastNameStartsWith(string $lastNameStartsWith) {
		$this->lastNameStartsWith = $lastNameStartsWith;
	}

	public function setModifiedSince(string $modifiedSince) {
		$this->modifiedSince = $modifiedSince;
	}

	public function setComics(string $comics) {
		$this->comics = $comics;
	}

	public function setSeries(string $series) {
		$this->series = $series;
	}

	public function setEvents(string $events) {
		$this->events = $events;
	}

	public function setStories(string $stories) {
		$this->stories = $stories;
	}
}
