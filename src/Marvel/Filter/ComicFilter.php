<?php

namespace App\Marvel\Filter;


class ComicFilter extends AbstractFilter {

	public $format;
	public $formatType;
	public $noVariants;
	public $dateDescriptor;
	public $dateRange;
	public $title;
	public $titleStartsWith;
	public $startYear;
	public $issueNumber;
	public $diamondCode;
	public $digitalId;
	public $upc;
	public $isbn;
	public $ean;
	public $issn;
	public $hasDigitalIssue;
	public $modifiedSince;
	public $creators;
	public $series;
	public $events;
	public $stories;
	public $sharedAppearances;
	public $collaborators;
	public $characters;

	public function getFormat(): string {
		return $this->format;
	}
	
	public function getFormatType(): string {
		return $this->formatType;
	}
	
	public function getNoVariants(): bool {
		return $this->noVariants;
	}
	
	public function getDateDescriptor(): string {
		return $this->dateDescriptor;
	}
	
	public function getDateRange(): int {
		return $this->dateRange;
	}
	
	public function getTitle(): string {
		return $this->title;
	}
	
	public function getTitleStartsWith(): string {
		return $this->titleStartsWith;
	}
	
	public function getStartYear(): int {
		return $this->startYear;
	}
	
	public function getIssueNumber(): int {
		return $this->issueNumber;
	}
	
	public function getDiamondCode(): string {
		return $this->diamondCode;
	}
	
	public function getDigitalId(): int {
		return $this->digitalId;
	}
	
	public function getUpc(): string {
		return $this->upc;
	}
	
	public function getIsbn(): string {
		return $this->isbn;
	}
	
	public function getEan(): string {
		return $this->ean;
	}
	
	public function getIssn(): string {
		return $this->issn;
	}
	
	public function getHasDigitalIssue(): bool {
		return $this->hasDigitalIssue;
	}
	
	public function getModifiedSince(): string {
		return $this->modifiedSince;
	}
	
	public function getCreators(): int {
		return $this->creators;
	}
	
	public function getCharacters(): string {
		return $this->characters;
	}
	
	public function getSeries(): string {
		return $this->series;
	}
	
	public function getEvents(): string {
		return $this->events;
	}
	
	public function getStories(): string {
		return $this->stories;
	}

	public function getSharedAppearances(): string {
		return $this->sharedAppearances;
	}

	public function getCollaborators(): string {
		return $this->collaborators;
	}

	public function setFormat(string $format) {
		$this->format = $format;
	}
	
	public function setFormatType(string $formatType) {
		$this->formatType = $formatType;
	}

	public function setNoVariants(bool $noVariants) {
		$this->noVariants = $noVariants;
	}

	public function setDateDescriptor(string $dateDescriptor) {
		$this->dateDescriptor = $dateDescriptor;
	}

	public function setDateRange(int $dateRange) {
		$this->dateRange = $dateRange;
	}

	public function setTitle(string $title) {
		$this->title = $title;
	}

	public function setTitleStartsWith(string $titleStartsWith) {
		$this->titleStartsWith = $titleStartsWith;
	}

	public function setStartYear(int $startYear) {
		$this->startYear = $startYear;
	}

	public function setIssueNumber(int $issueNumber) {
		$this->issueNumber = $issueNumber;
	}

	public function setDiamondCode(string $diamondCode) {
		$this->diamondCode = $diamondCode;
	}

	public function setDigitalId(int $digitalId) {
		$this->digitalId = $digitalId;
	}

	public function setUpc(string $upc) {
		$this->upc = $upc;
	}

	public function setIsbn(string $isbn) {
		$this->isbn = $isbn;
	}

	public function setEan(string $ean) {
		$this->ean = $ean;
	}

	public function setIssn(string $issn) {
		$this->issn = $issn;
	}

	public function setHasDigitalIssue(bool $hasDigitalIssue) {
		$this->hasDigitalIssue = $hasDigitalIssue;
	}

	public function setModifiedSince(string $modifiedSince) {
		$this->modifiedSince = $modifiedSince;
	}

	public function setCreators(string $creators) {
		$this->creators = $creators;
	}

	public function setCharacters(string $characters) {
		$this->characters = $characters;
	}

	public function setSeries(string $series) {
		$this->series = $series;
	}

	public function setEvents(string $events) {
		$this->events = $events;
	}

	public function setStories(string $stories) {
		$this->stories = $stories;
	}
	
	public function setSharedAppearances(string $sharedAppearances) {
		$this->sharedAppearances = $sharedAppearances;
	}

	public function setCollaborators(string $collaborators) {
		$this->collaborators = $collaborators;
	}

}