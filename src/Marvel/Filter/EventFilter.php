<?php

namespace App\Marvel\Filter;


class EventFilter extends AbstractFilter {

	public $name;
	public $nameStartsWith;
	public $modifiedSince;
	public $creators;
	public $characters;
	public $series;
	public $comics;
	public $stories;

	public function getName(): string {
		return $this->name;
	}

	public function getNameStartsWith(): string {
		return $this->nameStartsWith;
	}

	public function getModifiedSince(): string {
		return $this->modifiedSince;
	}

	public function getCreators(): string {
		return $this->creators;
	}

	public function getCharacters(): string {
		return $this->characters;
	}

	public function getSeries(): string {
		return $this->series;
	}

	public function getComics(): string {
		return $this->comics;
	}

	public function getStories(): string {
		return $this->stories;
	}


	public function setName(string $name) {
		$this->name = $name;
	}

	public function setNameStartsWith(string $nameStartsWith) {
		$this->nameStartsWith = $nameStartsWith;
	}

	public function setModifiedSince(string $modifiedSince) {
		$this->modifiedSince = $modifiedSince;
	}

	public function setCreators(string $creators) {
		$this->creators = $creators;
	}

	public function setCharacters(string $characters) {
		$this->characters = $characters;
	}

	public function setSeries(string $series) {
		$this->series = $series;
	}

	public function setComics(string $comics) {
		$this->comics = $comics;
	}

	public function setStories(string $stories) {
		$this->stories = $stories;
	}
}