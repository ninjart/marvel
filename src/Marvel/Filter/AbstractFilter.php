<?php

namespace App\Marvel\Filter;


class AbstractFilter {
	public $orderBy;
	public $limit;
	public $offset;

	public function getOrderBy(): string {
		return $this->orderBy;
	}

	public function getLimit(): int {
		return $this->limit;
	}

	public function getOffset(): int {
		return $this->offset;
	}

	public function setOrderBy(string $orderBy) {
		$this->orderBy = $orderBy;
	}

	public function setLimit(int $limit) {
		$this->limit = $limit;
	}

	public function setOffset(int $offset) {
		$this->offset = $offset;
	}
}