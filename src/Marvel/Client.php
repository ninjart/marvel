<?php

namespace App\Marvel;

use App\Marvel\Filter\AbstractFilter;
use GuzzleHttp\Client as RemoteClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;


class Client {

	/**
	 * @var string
	 */
	private $baseUrl = 'http://gateway.marvel.com/v1/public/';
	/**
	 * @var string
	 */
	private $publicApiKey;
	/**
	 * @var string
	 */
	private $privateApiKey;

	public function __construct(string $privateApiKey, string $publicApiKey)
	{
		$this->privateApiKey = $privateApiKey;
		$this->publicApiKey = $publicApiKey;
	}
	public function call(string $endpoint, AbstractFilter $filters = null) : Response
	{
		$url = $this->baseUrl . $endpoint;
		$params = array();
		if (!empty($filters)) {
			$params = get_object_vars($filters);
		}
		return $this->remote($url, $params);
	}

	private function remote(string $url, array $params = array()) : Response
	{
		$client = new RemoteClient();
		$timestamp =  time();

		$query = [
			'ts' => $timestamp,
			'apikey' => $this->publicApiKey,
			'hash' => md5($timestamp . $this->privateApiKey . $this->publicApiKey),
		];
		foreach (array_filter($params) as $key => $value) {
			$query[$key] = $value;
		}
		try {
			return $client->request('GET', $url, ['query' => $query]);
		} catch (GuzzleException $e) {
			if ($e->hasResponse()) {
				throw new ClientException($e->getMessage(), $e->getRequest());
			}
		}
	}

}