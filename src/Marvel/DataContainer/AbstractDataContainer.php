<?php

namespace App\Marvel\DataContainer;


abstract class AbstractDataContainer implements DataContainerInterface {

	private $offset;
	private $limit;
	private $total;
	private $count;

	public function getOffset(): int {
		return $this->offset;
	}

	public function getLimit(): int {
		return $this->limit;
	}

	public function getTotal(): int {
		return $this->total;
	}

	public function getCount(): int {
		return $this->count;
	}

	public function setOffset(int $offset) {
		$this->offset = $offset;
	}

	public function setLimit(int $limit) {
		$this->limit = $limit;
	}

	public function setTotal(int $total) {
		$this->total = $total;
	}

	public function setCount(int $count) {
		$this->count = $count;
	}
}