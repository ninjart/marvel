<?php

namespace App\Marvel\DataContainer;


use App\Marvel\Entity\Creator;

class CreatorDataContainer extends AbstractDataContainer {

	private $results;

	public function get($key) {
		return $this->results[$key];
	}

	public function getResults() {
		return $this->results;
	}

	public function insertResult(Creator $item) {
		$this->results[] = $item;
	}
}