<?php

namespace App\Marvel\DataContainer;


use App\Marvel\Entity\Event;

class EventDataContainer extends AbstractDataContainer {

	private $results;

	public function get($key) {
		return $this->results[$key];
	}

	public function getResults() {
		return $this->results;
	}

	public function insertResult(Event $item) {
		$this->results[] = $item;
	}
}