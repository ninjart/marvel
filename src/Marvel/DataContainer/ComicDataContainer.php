<?php

namespace App\Marvel\DataContainer;


use App\Marvel\Entity\Comic;

class ComicDataContainer extends AbstractDataContainer {

	private $results;

	public function get($key) {
		return $this->results[$key];
	}

	public function getResults() {
		return $this->results;
	}

	public function insertResult(Comic $item) {
		$this->results[] = $item;
	}
}