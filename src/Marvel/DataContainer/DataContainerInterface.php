<?php

namespace App\Marvel\DataContainer;


interface DataContainerInterface {

	public function getOffset() : int;

	public function getLimit() : int;

	public function getTotal() : int;

	public function getCount() : int;

	/**
	 * @return EntityInterface[]
	 */
//	public function getResults() : array;
}