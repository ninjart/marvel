<?php

namespace App\Marvel\DataContainer;


use App\Marvel\Entity\Character;

class CharacterDataContainer extends AbstractDataContainer {

	private $results;

	public function get($key) {
		return $this->results[$key];
	}

	public function getResults() {
		return $this->results;
	}

	public function insertResult(Character $item) {
		$this->results[] = $item;
	}
}