<?php

namespace App\Marvel\DataContainer;


use App\Marvel\Entity\Series;

class SeriesDataContainer extends AbstractDataContainer {

	private $results;

	public function get($key) {
		return $this->results[$key];
	}

	public function getResults() {
		return $this->results;
	}

	public function insertResult(Series $item) {
		$this->results[] = $item;
	}
}